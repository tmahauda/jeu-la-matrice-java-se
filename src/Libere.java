package personne;
import matrice.*;

/**
*La classe Libere permet de représenter un membre libéré de la Flotte Sion héritant de la classe Personne
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Libere extends Personne{
  private int nbrInfiltration;
  private boolean infiltrer, infecter;

  /**
  *Constructeur de la classe Libere permettant de créer un objet de type Libere
  *@param leNom qui represente le nom du membre libéré
  *@param leSexe qui represente le sexe du membre libéré
  *@param leGrade qui represente le grade du membre libéré
  */
  public Libere(String leNom, String leSexe, String leGrade){
    super(leNom, leSexe, leGrade);
    this.nbrInfiltration = 0; //nbrInfiltration qui représente le nombre d'infiltration dans la matrice du membre libéré : au début aucun
    this.infiltrer = false; //infiltre qui permet de savoir si un membre libéré est présent dans la matrice : au début non
    this.infecter = false; //infecte qui permet de savoir si un membre libéré est blessé dans la matrice : au début non
  }

  /**
  *Méthode qui permet d'infiltrer un membre libéré dans La Matrice à une case choisie
  *@param laMatrice qui représente la matrice à infiltrer
  *@param laCase qui représente la case de la matrice où l'on doit positionner le membre infiltré
  */
  public void infiltration(LaMatrice laMatrice, Case laCase){
    if(!this.infiltrer){
      this.nbrInfiltration++;
      this.infiltrer = true;
      laCase.setOccupant(this);
      laCase.setLibre(false);
      System.out.println("Infiltration réussie");
      if(!this.estInfecte(laMatrice)){
        laMatrice.agentPlusProche(this).diminuerDegre(laMatrice);
      }else{
        System.out.println("L'agent a été infecté");
      }
      laMatrice.updateVisible();
    }else{
      System.out.println("Ce membre est déjà infiltré");
    }
  }

  /**
  *Sélecteur qui permet de vérifier si un membre libéré est infiltré ou non dans La Matrice
  *@return si le membre libéré est infiltré ou non dans La Matrice
  */
  public boolean getInfiltre(){
    return this.infiltrer;
  }

  /**
  *Modificateur qui permet de modifier l'infiltration d'un membre libéré
  *@param leInfiltrer qui représente vrai ou faux
  */
  public void setInfiltre(boolean leInfiltrer){
    this.infiltrer = leInfiltrer;
  }

  /**
  *Sélecteur qui permet de vérifier si un membre libéré est infecté
  *@return si le membre libéré est infecté ou non
  */
  public boolean estInfecte(){
    return this.infecter;
  }

  /**
  *Méthode en surcharge qui permet de vérifier si un membre libéré est infecté ou non dans La Matrice
  *-s'il n'est pas infecté, le calcule de l'infection est fait
  *@param laMatrice qui represente La Matrice
  *@return si le membre libéré est infecté ou non dans La Matrice
  */
  public boolean estInfecte(LaMatrice laMatrice){
    if(!this.infecter){
      this.infecter = laMatrice.infection(this);
    }
    return this.infecter;
  }

  /**
  *Méthode redéfini de la classe Personne qui permet d'afficher la présence d'un membre libéré dans La Matrice en une chaîne de caractères
  *@return la lettre m si le membre est infecté ou M si non suivi de son nombre d'infiltration si celui-ci est supérieur à 5
  */
  public String affichageMatrice(){
    return ((this.infecter)? "m" : "M") + ((this.nbrInfiltration > 5)? "5" : this.nbrInfiltration);
  }

  /**
  *Convertisseur qui permet de représenter un membre libéré par son nom, son sexe, son grade, son nombre d'infiltration, sa présence et s'il est infecté dans La Matrice en une chaîne de caractères
  *@return le nom, le sexe, le grade, le nombre d'infiltration, la présence et s'il est infecté dans La Matrice en une chaîne de caractères
  */
  public String toString(){
    String infiltrer = "";
    String infecter = "";
    infiltrer = (this.infiltrer)? "actuellement infiltré et " + ((this.infecter)? "infecté" : "non-infecté") : "";
    return ("Membre libéré - "+super.toString()+"; nombre d'infiltration : "+this.nbrInfiltration+"; " + infiltrer);
  }

  /**
  *Sélecteur qui permet d'obtenir le nombre d'infiltration d'un membre libéré dans La Matrice
  *@return le nombre d'infiltration d'un membre libéré dans La Matrice
  */
  public int getNbrInfiltration(){
    return this.nbrInfiltration;
  }
}
