package personne;
import matrice.*;

/**
*La classe Agent permet de représenter un agent héritant de la classe Personne
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Agent extends Personne{
  private int degre;

  /**
  *Constructeur de la classe Agent permettant de créer un objet de type Agent
  *@param leNom qui représente le nom de l'agent
  *@param leSexe qui représente le sexe de l'agent
  *@param leGrade qui représente le grade de l'agent
  */
  public Agent(String leNom, String leSexe, String leGrade){
    super(leNom, leSexe, leGrade);
    this.degre = (int)(Math.random() * 5) + 1; //degre qui représente l'efficacité d'un agent choisit aléatoirement entre 1 et 5 bornes incluses
  }

  /**
  *Sélecteur qui permet d'obtenir le degré d'efficacité d'un agent
  *@return le degré de l'agent
  */
  public int getDegre(){
    return this.degre;
  }

  /**
  *Méthode qui permet de diminiuer le degré d'efficacité d'un agent par deux s'il n'a pas été déjà neutralisé dans La Matrice
  *@param laMatrice qui représente La Matrice
  */
  public void diminuerDegre(LaMatrice laMatrice){
    if(this.degre == 0){
      System.out.println("L'agent le plus proche est déjà neutralisé");
    }else{
      this.degre /= 2;
      if(this.degre == 0){
        System.out.println("L'agent le plus proche a été neutralisé");
      }else{
        System.out.println("L'agent le plus proche a été affaiblit");
      }
    }
  }

  /**
  *Méthode redéfini de la classe Personne qui permet d'afficher la présence d'un agent dans la matrice suivi de son degré d'efficacité
  *@return la lettre A suivi de son degré d'efficacité
  */
  public String affichageMatrice(){
    return "A"+this.degre;
  }

  /**
  *Convertisseur en une chaîne de caractères qui permet de représenter un agent par son nom, son sexe, son grade et son dégré d'efficacité
  *@return le nom, le sexe, le grade et le degré d'efficacité de l'agent en une chaîne de caractères
  */
  public String toString(){
    return ("Agent : "+super.toString()+"; degré d'efficacité : "+this.degre);
  }

  /**
  *Méthode qui permet d'upgrader la promotion d'un agent en augmentant son degré d'efficacité si celui-ci n'est ni supérieur à 5 et ni égal à 0
  */
  public void promotion(){
    if(degre < 5 && degre != 0){
      degre++;
    }
  }

}
