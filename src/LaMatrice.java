package matrice;
import personne.*;

/**
*La classe LaMatrice permet de représenter La Matrice dans laquelle vont intéragir des membres libérés infiltrés et des agents
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class LaMatrice{
  private Case caseT[][];
  private int nbAgent;

  /**
  *Constructeur de la classe LaMatrice permettant de créer un objet de type LaMatrice
  *@param laDifficulte qui représente la difficulté de La Matrice
  *-si la difficulté est supérieur à 3, alors on sélectionne 5 agents
  *-sinon on sélectionne n (n = difficulté choisi + 2) agents
  */
  public LaMatrice(int laDifficulte){
    caseT = new Case[10][10];
    this.nbAgent = (laDifficulte > 3)? 5 : laDifficulte + 2;
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        caseT[i][j] = new Case(i, j);
      }
    }
    int i = 0;
    while(i < this.nbAgent){
      int x = (int)(Math.random() * 10);
      int y = (int)(Math.random() * 10);
      if(caseT[x][y].getLibre()){
        caseT[x][y].setOccupant(new Agent("agent_" + i, "Homme", "Agent"));
        caseT[x][y].setLibre(false);
        i++;
      }
    }
  }

  /**
  *Sélecteur qui permet de récupérer une case de La Matrice par ses coordonnées
  *@param leX qui représente la coordonnée sur l'axe des abscisses Ox de la case
  *@param leY qui représente la coordonnée sur l'axe des ordonnées Oy de la case
  *@return la case de La Matrice
  */
  public Case getCase(int leX, int leY){
    return caseT[leX][leY];
  }

  /**
  *Sélecteur qui permet de récupérer une case de La Matrice par un agent qui l'occupe
  *@param leAgent qui représente l'agent qui occupe la case
  *@return la case de La Matrice
  */
  public Case getCase(Agent leAgent){
    boolean trouver = false;
    int j = 0;
    Case retour = null;
    while(j < 10){
      int i = 0;
      while(i < 10){
        if(caseT[i][j].getOccupant() instanceof Agent){
          Agent a = (Agent)caseT[i][j].getOccupant();
          if(a == leAgent){
            trouver = true;
            retour = caseT[i][j];
          }
        }
        i++;
      }
      j++;
    }
    return retour;
  }

  /**
  *Méthode qui permet d'exfiltrer un membre libéré de La Matrice s'il n'est pas infecté
  *@param leLibere qui represente le membre libéré à exfiltrer
  */
  public void exfiltrer(Libere leLibere){
    if(!leLibere.estInfecte(this)){
      boolean trouver = false;
      int j = 0;
      while(j < 10 && !trouver){
        int i = 0;
        while(i < 10 && !trouver){
          if(caseT[i][j].getOccupant() == leLibere){
            trouver = true;
            caseT[i][j].setOccupant(null);
            caseT[i][j].setLibre(true);
            leLibere.setInfiltre(false);
          }
          i++;
        }
        j++;
      }
      if(trouver){
        this.updateVisible();
        System.out.println("Exfiltration réussie");
      }
    }else{
      System.out.println("Ce membre libéré est infecté, vous ne pouvez plus l'exfiltrer");
    }
  }

  /**
  *Méthode qui permet d'afficher les membres libérés infiltrés dans La Matrice en une chaîne de caractères
  *Chauqe membre est précédé d'un numéro afin que l'utilisateur puisse en choisir un rapidement
  *@return les membres libérés infiltrés dans La Matrice en une chaîne de caractères
  */
  public String displayInfiltre(){
    String affiche = "Les membres libérés infiltrés sont : \n";
    int compteur = 0;
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        if((!caseT[i][j].getLibre()) && caseT[i][j].getOccupant() instanceof Libere){
          affiche += (++compteur) + " - " + caseT[i][j].getOccupant().toString()+"; position ("+i+","+j+")\n";
        }
      }
    }
    return affiche;
  }

  /**
  *Sélecteur qui permet d'obtenir un membre libéré infiltré dans La Matrice à partir de son numero
  *@param numero qui représente son numéro, correspondant au numéro affiché dans la méthode displayInfiltre
  *@return le membre libéré infiltré
  */
  public Libere getInfiltre(int numero){
    int compteur = 0;
    Libere retour = null;
    int j = 0;
    while(j < 10 && compteur != numero){
      int i = 0;
      while(i < 10 && compteur != numero){
        if((!caseT[i][j].getLibre()) && caseT[i][j].getOccupant() instanceof Libere){
          if(++compteur == numero){
            retour = (Libere)caseT[i][j].getOccupant();
          }
        }
        i++;
      }
      j++;
    }
    return retour;
  }

  /**
  *Méthode qui permet d'obtenir le nombre de membre exfiltrable de La Matrice
  *@return le nombre de membre exfiltrable de La Matrice
  */
  public int nbExfiltrable(){
    int retour = 0;
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        if((!caseT[i][j].getLibre()) && caseT[i][j].getOccupant() instanceof Libere){
          Libere l = (Libere)caseT[i][j].getOccupant();
          if(! l.estInfecte()){
            retour++;
          }
        }
      }
    }
    return retour;
  }

  /**
  *Méthode qui permet d'obtenir le nombre de membre infiltré dans La Matrice
  *@return le nombre de membre infiltré dans La Matrice
  */
  public int nbInfiltre(){
    int retour = 0;
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        if((!caseT[i][j].getLibre()) && caseT[i][j].getOccupant() instanceof Libere){
          retour++;
        }
      }
    }
    return retour;
  }

  /**
  *Méthode qui permet d'afficher si la flotte Sion a remporté la bataille ou non en une chaîne de caractères
  *@return le résultat de la victoire en une chaîne de caractères
  */
  public String displayVictoire(){
    boolean victoire = true;
    int j = 0;
    while(j < 10 && victoire){
      int i = 0;
      while(i < 10 && victoire){
        if((!caseT[i][j].getLibre()) && caseT[i][j].getOccupant() instanceof Agent){
          Agent a = (Agent)caseT[i][j].getOccupant();
          if(a.getDegre() != 0){
            victoire = false;
          }
        }
        i++;
      }
      j++;
    }
    String resultat = "";
    if(victoire){
      resultat = "La flotte Sion a gagnée";
    }else{
      resultat = "La flotte de Sion a perdue";
    }
    return resultat;
  }

  /**
  *Méthode qui permet de calculer la distance entre un membre libéré infiltré et un agent dans La Matrice
  *@param leLibere qui represente le membre libéré infiltré
  *@param leAgent qui represente l'agent
  *@return la distance entre le membre libéré infiltré et l'agent
  */
  public int distanceAgent(Libere leLibere, Agent leAgent){
    int distance = 0;
    if(! leLibere.estInfecte()){
      int x = 0;
      int y = 0;
      int j = 0;
      boolean trouver = false;
      while(j < 10 && !trouver){
        int i = 0;
        while(i < 10 && !trouver){
          if(caseT[i][j].getOccupant() == leLibere){
            trouver = true;
            x = i;
            y = j;
          }
          i++;
        }
        j++;
      }
      if(trouver){
        j = 0;
        trouver = false;
        while(j < 10 && !trouver){
          int i = 0;
          while(i < 10 && !trouver){
            if(caseT[i][j].getOccupant() == leAgent){
              trouver = true;
              distance = (x > i)? x-i: i-x;
              distance += (y > j)? y-j: j-y;
            }
            i++;
          }
          j++;
        }
        if(!trouver){
          System.out.println("Cet agent n'est pas présent dans la matrice");
        }
      }else{
        System.out.println("Ce membre n'est pas infiltré dans la matrice");
      }
    }
    return distance;
  }

  /**
  *Méthode qui permet de vérifier si un membre libéré infiltré dans La Matrice est infecté ou non
  *@param leLibere qui représente le membre libéré infiltré
  *@return si le membre libéré infiltré est infecté ou non
  */
  public boolean infection(Libere leLibere){
    int x = 0;
    int y = 0;
    int j = 0;
    boolean trouver = false;
    boolean retour = false;
    while(j < 10 && !trouver){
      int i = 0;
      while(i < 10 && !trouver){
        if(caseT[i][j].getOccupant() == leLibere){
          trouver = true;
          x = i;
          y = j;
        }
        i++;
      }
      j++;
    }
    j = 0;
    while(j < 10 && ! retour){
      int i = 0;
      while(i < 10 && ! retour){
        if(caseT[i][j].getOccupant() instanceof Agent){
          Agent a = (Agent)caseT[i][j].getOccupant();
          retour = ((a.getDegre()/this.distanceAgent(leLibere, a)) > leLibere.getNbrInfiltration());
        }
        i++;
      }
      j++;
    }
    return retour;
  }

  /**
  *Méthode qui permet de trouver un agent le plus proche par rapport à un membre libéré infiltré dans la matrice
  *@param leLibere qui represente le membre libéré infiltré
  *@return l'agent le plus proche du membre libéré infiltré
  */
  public Agent agentPlusProche(Libere leLibere){
    Case c = new Case(0,0);
    int j = 0;
    int distance = 20;
    while(j < 10){
      int i = 0;
      while(i < 10){
        if(caseT[i][j].getOccupant() instanceof Agent){
          Agent a = (Agent)this.caseT[i][j].getOccupant();
          int test = this.distanceAgent(leLibere,a);
          if(test < distance){
            c = caseT[i][j];
            distance = test;
          }
        }
        i++;
      }
      j++;
    }
    return (Agent)c.getOccupant();
  }

  /**
  *Convertisseur qui permet d'afficher La Matrice en une chaîne de caractères
  *@return La Matrice en une chaîne de caractères
  */
  public String toString(){
    String affiche = "    ";
    for(int i = 0; i < 10; i++){
      affiche += "  " + i + " ";
    }
    affiche += "  x\n";
    for(int j = 0; j < 10; j++){
      affiche += "    ";
      for(int i = 0; i < 10; i++){
        affiche += "+---";
      }
      affiche += "+\n  " + j + " ";
      for(int i = 0; i < 10; i++){
        affiche += "|" + caseT[i][j].toString();
      }
      affiche += "|\n";
    }
    affiche += "    ";
    for(int i = 0; i < 10; i++){
      affiche += "+---";
    }
    affiche += "+\n  y\n";
    return affiche;
  }

  /**
  *Méthode qui permet de mettre à jour la visibilité de La Matrice
  */
  public void updateVisible(){
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        caseT[i][j].setVisible(false);
      }
    }
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        if(caseT[i][j].getOccupant() instanceof Libere){
          Libere l = (Libere)caseT[i][j].getOccupant();
          if(! l.estInfecte(this)){
            int distance = (l.getNbrInfiltration() > 5)? 5 : l.getNbrInfiltration() ;
            for(int y = 1; y <= distance; y++){
              for(int x = y - distance ; x <= distance - y; x++){
                if(i+x < 10 && i+x >= 0 && j+y < 10 && j+y >= 0){
                  caseT[i + x][j + y].setVisible(true);
                }
                if(i+x < 10 && i+x >= 0 && j-y < 10 && j-y >= 0){
                  caseT[i + x][j - y].setVisible(true);
                }
              }
            }
            for(int x = -distance; x <= distance; x++){
              if(i+x < 10 && i+x >= 0){
                caseT[i + x][j].setVisible(true);
              }
            }
          }
        }
      }
    }
  }

  /**
  *Méthode qui permet de choisir la promotion d'un agent aléatoirement
  */
  public void promotionAleatoire(){
    int n = (int)(Math.random() * (this.nbAgent)) + 1;
    int compteur = 0;
    int j = 0;
    while(j < 10 && compteur != n){
      int i = 0;
      while(i < 10 && compteur != n){
        if(caseT[i][j].getOccupant() instanceof Agent){
          compteur++;
          if(compteur == n){
            Agent a = (Agent)caseT[i][j].getOccupant();
            a.promotion();
          }
        }
        i++;
      }
      j++;
    }
  }

  /**
  *Sélecteur qui permet d'obtenir le nombre d'agent présent dans La matrice
  *@return le nombre d'agent présent dans La Matrice
  */
  public int getNbAgent(){
    return this.nbAgent;
  }

  /**
  *Sélecteur qui permet d'obtenir le nombre d'agent présent dans La matrice et n'étant pas encore maitrisé
  *@return le nombre d'agent présent dans La Matrice
  */
  public int getNbAgentOperationnel(){
    int compteur=0;
    for(int j = 0; j < 10; j++){
      for(int i = 0; i < 10; i++){
        if(caseT[i][j].getOccupant() instanceof Agent){
          Agent a = (Agent)caseT[i][j].getOccupant();
          if(a.getDegre() != 0){
            compteur++;
          }
        }
      }
    }
    return compteur;
  }

}
