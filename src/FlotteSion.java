package vaisseau;
import liste.Liste;
import personne.*;
import matrice.*;

/**
*La classe FlotteSion permet de représenter une liste de vaisseau
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class FlotteSion{
  private Liste vaisseau;

  /**
  *Constructeur de la classe FlotteSion permettant de créer un objet de type FlotteSion
  */
  public FlotteSion(){
    this.vaisseau = new Liste(); //vaisseau qui représente la liste de vaisseaux présent dans la flotte Sion
  }

  /**
  *Méthode qui permet d'ajouter un vaisseau dans la flotte Sion
  *@param leVaisseau qui représente le vaisseau à ajouter
  */
  public void addVaisseau(Vaisseau leVaisseau){
    boolean trouver = false;
    int i = 0;
    while(i < this.vaisseau.size() && !trouver){
      trouver = this.vaisseau.get(i++) == leVaisseau;
    }
    if(!trouver){
      this.vaisseau.add(leVaisseau);
    }else{
      System.out.println("Ce vaisseau est déjà dans la FlotteSion");
    }
  }

  /**
  *Méthode qui permet de supprimer un vaisseau dans la flotte Sion
  *@param leVaisseau qui représente le vaisseau à supprimer
  */
  public void removeVaisseau(Vaisseau leVaisseau){
    this.vaisseau.remove(leVaisseau);
  }

  /**
  *Convertisseur en une chaîne de caractères qui permet d'afficher les vaisseaux présent dans la flotte Sion avec son nom et son type
  *@return tous les vaisseaux présent dans la flotte Sion en une chaîne de caractères
  */
  public String toString(){
    String flotte = "La flotte comprend le ou les vaisseaux suivants : \n";
    for(int i=0; i<this.vaisseau.size(); i++){
      flotte += ((Vaisseau)this.vaisseau.get(i)).toString()+"\n";
    }
    return flotte;
  }

  /**
  *Méthode qui permet d'afficher les membres de la flotte Sion en une chaîne de caractères
  *@return les membres de la flotte Sion en une chaîne de caractères
  */
  public String displayMembre(){
    String membre = "";
    for(int i = 0; i < this.vaisseau.size(); i++){
      membre += ((Vaisseau)this.vaisseau.get(i)).displayMembre() + "\n";
    }
    return membre;
  }

  /**
  *Méthode qui permet d'afficher les membres de la flotte Sion capable d'infiltrer La Matrice en une chaîne de caractères
  *Chaque membre afficher sera précédé d'un numéro afin que le joueur puisse en choisir un rapidements
  *@return les membres de la flotte Sion capable d'infiltrer La Matrice en une chaîne de caractères
  */
  public String displayInfiltrable(){
    String retour = "";
    int compteur = 0;
    for(int i = 0; i < this.vaisseau.size(); i++){
      Vaisseau v = (Vaisseau)this.vaisseau.get(i);
      retour += "\nDans " + v.toString() + " :\n";
      for(int j = 0; j < v.getTailleEquipage(); j++){
        if(v.getMembre(j) instanceof Libere){
          Libere l = (Libere)v.getMembre(j);
          if(! l.getInfiltre()){
            compteur++;
            retour += compteur + " - " + l.toString() + "\n";
          }
        }
      }
    }
    return retour;
  }

  /**
  *Sélecteur qui permet d'obtenir un membre de la flotte Sion à partir d'un indice capable d'infiltrer La Matrice
  *@param numero qui représente le numéro du membre dans l'affichage de la méthode displayInfiltrable
  *@return le membre capable d'infiltrer La Matrice
  */
  public Libere getInfiltrable(int numero){
    int compteur = 0;
    boolean trouver = false;
    int i = 0;
    Libere retour = null;
    while(i < this.vaisseau.size() && !trouver){
      Vaisseau v = (Vaisseau)this.vaisseau.get(i);
      int j = 0;
      while(j < v.getTailleEquipage() && !trouver){
        if(v.getMembre(j) instanceof Libere){
          Libere l = (Libere)v.getMembre(j);
          if(! l.getInfiltre()){
            compteur++;
            if(compteur == numero){
              trouver = true;
              retour = l;
            }
          }
        }
        j++;
      }
      i++;
    }
    return retour;
  }

  /**
  *Méthode qui permet d'obtenir le nombre de membre capable d'infiltrer La Matrice de la flotte Sion
  *@return le nombre de membre capable d'infiltrer La Matrice de la flotte Sion
  */
  public int nbInfiltrable(){
    int retour = 0;
    for(int i = 0; i < this.vaisseau.size(); i++){
      Vaisseau v = (Vaisseau)this.vaisseau.get(i);
      for(int j = 0; j < v.getTailleEquipage(); j++){
        if(v.getMembre(j) instanceof Libere){
          Libere l = (Libere)v.getMembre(j);
          if(! l.getInfiltre()){
            retour++;
          }
        }
      }
    }
    return retour;
  }

  /**
  *Méthode qui permet d'infiltrer un membre libéré de la flotte Sion dans La Matrice
  *@param leLibere qui représente le membre libéré de la flotte Sion
  *@param laMatrice qui représente La Matrice à infiltrer
  *@param x qui représente la coordonnée en abcsisse dans la matrice où le membre libéré s'infiltre
  *@param y qui représente la coordonnée en ordonnée dans la matrice où le membre libéré s'infiltre
  */
  public void infiltrerMembre(Libere leLibere, LaMatrice laMatrice, int x, int y){
    boolean trouver = false;
    int i = 0;
    while(i < this.vaisseau.size() && !trouver){
      Vaisseau v = (Vaisseau)this.vaisseau.get(i++);
      if(v.checkMembre(leLibere)){
        trouver = true;
        v.infiltrerMembre(leLibere, laMatrice, x, y);
      }
    }
  }

  /**
  *Méthode qui permet d'obtenir le nombre de membre opérationnel de la flotte Sion
  *@return le nombre de membre opérationnel de la flotte Sion
  */
  public int nbMembreOperationnel(){
    int compteur = 0;
    for(int i = 0; i < this.vaisseau.size(); i++){
      Vaisseau v = (Vaisseau)this.vaisseau.get(i);
      for(int j = 0; j < v.getTailleEquipage(); j++){
        if(v.getMembre(j) instanceof Libere){
          Libere l = (Libere)v.getMembre(j);
          if(! l.estInfecte()){
            compteur++;
          }
        }
      }
    }
    return compteur;
  }

}
