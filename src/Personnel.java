package personne;
import liste.Liste;

/**
*La classe Personnel permet de stocker des personnes
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Personnel{
  private Liste personnel;

  /**
  *Constructeur de la classe Personnel permettant de créer un objet de type Personnel
  */
  public Personnel(){
    this.personnel = new Liste(); //personnel qui représente la liste des personnes à stocker
  }

  /**
  *Méthode qui permet d'ajouter une personne, exclus des agents, dans la liste du personnel
  *@param laPersonne qui représente la personne à ajouter
  */
  public void addPersonne(Personne laPersonne){
    if(!(laPersonne instanceof Agent)){
      if(!(this.checkPersonne(laPersonne))){
        this.personnel.add(laPersonne);
      }else{
        System.out.println("Vous ne pouvez pas ajouter des agents dans la liste du personnel");
      }
    }else{
      System.out.println("Cette personne est déjà ajouté dans la liste");
    }
  }

  /**
  *Méthode qui permet de supprimer une personne dans la liste du personnel
  *@param laPersonne qui représente la personne à supprimer
  */
  public void removePersonne(Personne laPersonne){
    this.personnel.remove(laPersonne);
  }

  /**
  *Méthode qui permet de vérifier si une personne est déjà présente ou non dans la liste du personnel
  *@param laPersonne qui represente la personne à vérifier
  *@return si la personne est présente ou non dans la liste du personnel
  */
  public boolean checkPersonne(Personne laPersonne){
    boolean trouver = false;
    int i = 0;
    while(i < this.personnel.size() && !trouver){
      trouver = this.personnel.get(i++) == laPersonne;
    }
    return trouver;
  }

  /**
  *Sélecteur qui permet d'obtenir la taille de la liste du personnel
  *@return la taille de la liste
  */
  public int getTaille(){
    return this.personnel.size();
  }

  /**
  *Méthode qui permet d'obtenir une personne membre à partir de son indice dans la liste
  *@param i qui represente l'indice de la liste où trouver la personne que l'on recherche
  *@return la personne
  */
  public Personne getMembre(int i){
    return (Personne)this.personnel.get(i);
  }

  /**
  *Convertisseur qui permet de lister toutes les personnes présentent dans la liste du personnel en une chaîne de caractères
  *@return les personnes présentent dans la liste personnel en une chaîne de caractères
  */
  public String toString(){
    String affiche = "";
    if(personnel.size() != 0){
      for(int i = 0; i < personnel.size(); i++){
        affiche += personnel.get(i).toString() + "\n";
      }
    }else{
      affiche = "Aucune personne n'est présente dans liste";
    }
    return affiche;
  }
}
