package personne;

/**
*La classe Operateur permet de représenter un opérateur de la flotte Sion héritant de la classe Personne
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Operateur extends Personne{
  private String secteur;

  /**
  *Constructeur de la classe Operateur permettant de créer un objet Operateur
  *@param leNom qui représente le nom de l'opérateur
  *@param leSexe qui représente le sexe de l'opérateur
  *@param leGrade qui représente le grade de l'opérateur
  *@param leSecteur qui représente le secteur de l'opérateur dont il s'occupe
  */
  public Operateur(String leNom, String leSexe, String leGrade, String leSecteur){
    super(leNom, leSexe, leGrade);
    this.secteur = leSecteur;
  }

  /**
  *Convertisseur qui permet de représenter un opérateur par son nom, son sexe, son grade et son secteur en une chaîne de caractères
  *@return le nom, le sexe, le grade et le secteur de l'opérateur en une chaîne de caractères
  */
  public String toString(){
    return ("Opérateur - "+super.toString()+"; secteur : "+this.secteur);
  }

  /**
  *Méthode redéfini de la classe Personne qui permet d'afficher la présence d'un opérateur dans La Matrice en une chaîne de caractères
  *Cette méthode est redéfinie car toute personne a une représentation dans La Matrice, mais un opérateur ne peut pas s'infiltrer dans La Matrice pour autant
  *@return la lettre O
  */
  public String affichageMatrice(){
    return "O";
  }

}
