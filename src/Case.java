package matrice;
import personne.*;

/**
*La classe Case permet de positionner des membres libérés ou des agents dans La Matrice
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Case{
  private int x, y;
  private Personne occupant;
  private boolean libre, visible;

  /**
  *Constructeur de la classe Case permettant de créer un objet de type Case pour composer La Matrice
  *@param leX qui représente la coordonnée sur l'axe des abscisses Ox
  *@param leY qui représente la coordonnée sur l'axe des ordonnées Oy
  */
  public Case(int leX, int leY){
    this(leX,leY,false);
  }

  /**
  *Constructeur en surcharge de la classe Case permettant de créer un objet de type Case pour composer La Matrice
  *@param leX qui représente la coordonnée sur l'axe des abscisses Ox
  *@param leY qui représente la coordonnée sur l'axe des ordonnées Oy
  *@param laVisible qui représente la visibilité de la case
  */
  public Case(int leX, int leY, boolean laVisible){
    this.x = leX;
    this.y = leY;
    this.libre = true;
    this.visible = laVisible;
    this.occupant = null;
  }


  /**
  *Sélecteur qui permet d'obtenir la disponibilité d'une case de La Matrice
  *@return l'état de la case
  */
  public boolean getLibre(){
    return this.libre;
  }

  /**
  *Modificateur qui permet de changer l'état d'une case de La Matrice
  *@param leEtat qui repésente vrai ou faux
  */
  public void setLibre(boolean leEtat){
    this.libre = leEtat;
  }

  /**
  *Modificateur qui permet de changer l'occupation d'une case de La Matrice par une personne
  *@param laPersonne qui représente la personne
  */
  public void setOccupant(Personne laPersonne){
    this.occupant = laPersonne;
  }

  /**
  *Méthode qui permet de connaitre l'occupant d'une case de La Matrice
  *@return la personne qui occupe la case
  */
  public Personne getOccupant(){
    return this.occupant;
  }

  /**
  *Convertisseur en une chaîne de caractères qui permet d'afficher ou non les personnes qui occupent des cases de La Matrice
  *@return le contenu des cases en une chaîne des caractères
  */
  public String toString(){
    String affiche = " ";
    if(this.visible){
      if(this.libre){
        affiche += "  ";
      }else{
        affiche += this.occupant.affichageMatrice();
      }
    }else{
      affiche += "~ ";
    }
    return affiche;
  }

  /**
  *Sélecteur qui permet de changer la visibité d'une case de La Matrice
  *@param laVisible qui repésente vrai ou faux
  */
  public void setVisible(boolean laVisible){
    this.visible = laVisible;
  }

}
