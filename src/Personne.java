package personne;

/**
*La super classe abstraite Personne permet de représenter une personne qui peut être soit un Agent, un Operateur ou un Libere
*Elle est non instanciable
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public abstract class Personne{
  private String nom, sexe, grade;

  /**
  *Constructeur de la super classe abstraite Personne permettant de factoriser les sous-classes héritant de celle-ci
  *@param leNom qui représente le nom de la personne
  *@param leSexe qui représente le sexe de la personne
  *@param leGrade qui représente le grade de la personne
  */
  public Personne(String leNom, String leSexe, String leGrade){
    this.nom = leNom;
    this.sexe = leSexe;
    this.grade = leGrade;
  }

  /**
  *Méthode abstraite qui permettra d'afficher la personne dans La Matrice par une lettre qui lui est propre
  *@return la lettre de la personne
  */
  public abstract String affichageMatrice();

  /**
  *Convertisseur qui permet de représenter une personne par son nom, son sexe et son grade en une chaîne de caractères
  *@return le nom, le sexe et le grade de la personne en une chaîne de caractères
  */
  public String toString(){
    return ("nom : "+this.nom+"; sexe : "+this.sexe+"; grade : "+this.grade);
  }

}
