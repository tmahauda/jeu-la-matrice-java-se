package vaisseau;
import liste.Liste;
import personne.*;
import matrice.*;

/**
*La classe Vaisseau permet de gérer un équipage
*
*@author MAHAUDA Théo, KOWALSKI Vincent, JOUINOT Damien
*@version 1.0
*/
public class Vaisseau{

  private String nom, type;
  private Liste equipage;

  /**
  *Constructeur de la classe Vaisseau permettant de créer un objet de type Vaisseau
  *@param leNom qui représente le nom du vaisseau
  *@param leType qui représente le type du vaisseau
  */
  public Vaisseau(String leNom, String leType){
    this.nom = leNom;
    this.type = leType;
    this.equipage = new Liste(); //equipage qui représente la liste d'équipage présente dans le vaisseau, c'est à dire les membres, exclus des agents
  }

  /**
  *Méthode qui permet d'ajouter un membre non agent dans la liste d'équipage, limité à 5 membres par vaisseau
  *@param leMembre qui représente le membre à ajouter
  *@param lePersonnel qui représente la vérification
  */
  public void addMembre(Personne leMembre, Personnel lePersonnel){
    if(equipage.size() < 5){
      if(!(leMembre instanceof Agent)){
        for(int i=0; i<lePersonnel.getTaille(); i++){
          if(lePersonnel.getMembre(i) == leMembre){
            this.equipage.add(leMembre);
          }
        }
      }else{
        System.out.println("Vous ne pouvez pas ajouter des agents dans des vaisseaux");
      }
    }else{
      System.out.println("Ce vaisseau est déjà plein");
    }
  }

  /**
  *Méthode qui permet de supprimer un membre dans la liste d'équipage
  *@param leMembre qui represente le membre à supprimer
  */
  public void removeMembre(Personne leMembre){
    if(this.checkMembre(leMembre)){
      this.equipage.remove(leMembre);
    }else{
      System.out.println("Le membre que vous voulez supprimer n'existe pas");
    }
  }

  /**
  *Méthode qui permet de vérifier si le membre est présent dans la liste d'équipage du vaisseau
  *@param leMembre qui représente le membre à trouver
  *@return si le membre est présent ou non dans la liste d'équipage
  */
  public boolean checkMembre(Personne leMembre){
    boolean trouver = false;
    int i = 0;
    while(i < this.equipage.size() && !(trouver)){
      trouver = this.equipage.get(i++) == leMembre;
    }
    return trouver;
  }

  /**
  *Méthode qui permet de vérifier si un vaisseau est sécurisé ou non avec la présence d'un opérateur
  *@return si le vaisseau est sécurisé ou non
  */
  public boolean checkSecuriser(){
    boolean securiser = false;
    int i = 0;
    while(i < this.equipage.size() && !(securiser)){
      securiser = this.equipage.get(i++) instanceof Operateur;
    }
    return securiser;
  }

  /**
  *Méthode qui permet d'infiltrer un membre libéré dans La Matrice à partir des coordonnées x et y s'il fait partie de l'équipage et s'il est dans un vaisseau sécurisé
  *@param leLibere qui represente le membre libéré à infiltrer
  *@param laMatrice qui represente La Matrice dans laquelle on infiltre le membre libéré
  *@param leX qui représente la coordonnée sur l'axe des abscisses Ox
  *@param leY qui représente la coordonnée sur l'axe des ordonnées Oy
  */
  public void infiltrerMembre(Libere leLibere, LaMatrice laMatrice, int leX, int leY){
    if(this.checkSecuriser()){
      if(this.checkMembre(leLibere)){
          if(laMatrice.getCase(leX, leY).getLibre()){
            leLibere.infiltration(laMatrice, laMatrice.getCase(leX, leY));
          }else{
            System.out.println("La case choisie est déjà occupée");
            System.out.println("Les données de ce membre ont été perdues dans le flux de données");
            System.out.println("Vous venez de perdre un membre");
            this.removeMembre(leLibere);
          }
      }
      else{
        System.out.println("Ce membre ne fait pas parti de l'équipage");
      }
    }
    else{
      System.out.println("Ce vaisseau n'est pas sécurisé");
    }
  }

  /**
  *Méthode qui permet d'afficher les membres inscrit dans la liste d'équipage en une chaîne de caractères
  *@return les membres de la liste d'équipage en une chaîne de caractères
  */
  public String displayMembre(){
    String membre = this.toString()+" contient les membres suivants :\n";
    for(int i=0;i<this.equipage.size();i++){
      membre += this.equipage.get(i).toString() + "\n";
    }
    return membre;
  }

  /**
  *Sélecteur qui permet d'obtenir la taille de l'équipage dans un vaisseau
  *@return la taille de l'équipage dans le vaisseau
  */
  public int getTailleEquipage(){
    return this.equipage.size();
  }

  /**
  *Sélecteur qui permet d'obtenir un membre du vaisseau inscrit dans la liste d'équipage
  *@param i qui représente l'indice où trouver le membre dans la liste d'équipage
  *@return le membre du vaisseau
  */
  public Personne getMembre(int i){
    Personne p = null;
    if(i < this.equipage.size()){
      p = (Personne)this.equipage.get(i);
    }else{
      System.out.println("La liste ne contient pas de " + i +"ème élément");
    }
    return p;
  }

  /**
  *Convertisseur en une chaîne de caractères qui permet de représenter un vaisseau par son nom et son type
  *@return le nom et le type du vaisseau en une chaîne de caractères
  */
  public String toString(){
    return ("le vaisseau "+this.nom+" de type "+this.type);
  }

}
