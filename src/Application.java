import liste.*;
import java.lang.Object;
import java.io.*;
import personne.*;
import vaisseau.*;
import matrice.*;

class Application{
  public static void main(String[] args){

    clearScreen();

    System.out.println(" _             __  __       _        _           ");
    System.out.println("| |           |  \\/  |     | |      (_)          ");
    System.out.println("| |     __ _  | \\  / | __ _| |_ _ __ _  ___ ___  ");
    System.out.println("| |    / _` | | |\\/| |/ _` | __| '__| |/ __/ _ \\ ");
    System.out.println("| |___| (_| | | |  | | (_| | |_| |  | | (_|  __/ ");
    System.out.println("|______\\__,_| |_|  |_|\\__,_|\\__|_|  |_|\\___\\___| \n");


    System.out.println("Il est temps de conquérir la Matrice !\n");
    System.out.println("Veuillez choisir la difficulté de jeu (1,2,3): ");
    boolean check = false;
    int entree1 = 0;
    while(! check){
    entree1 = Clavier.readInt();
    if(entree1 < 4 && entree1 > 0){
    check = true;
     switch(entree1){
       case 1 :
         if(entree1==1){
           System.out.println("Vous avez choisi le mode facile, il y aura 3 agents ennemis, bonne chance.\n");
         }
         break;
       case 2 :
         if(entree1==2){
           System.out.println("Vous avez choisi le mode intermédiaire, il y aura 4 agents ennemis, bonne chance.\n");
         }
         break;
       case 3 :
         if(entree1==3){
           System.out.println("Vous avez choisi le mode hardcore, il y aura 5 agents ennemis, bonne chance.\n");
         }
         break;
     }
    }
    else{
      System.out.println("Cette difficulté n'existe pas...saisissez une des difficultés existante (1,2,3)");
    }
    }

    System.out.println("Création de la matrice en cours...\n");
    try{
    Thread.sleep(4000);}
    catch(InterruptedException e){
    }
    clearScreen();
    LaMatrice matrice = new LaMatrice(entree1);
    Personnel personnel = new Personnel();
    FlotteSion flotte = new FlotteSion();
    Vaisseau vaisseau1 = new Vaisseau("Falcon","Guerre");
    Vaisseau vaisseau2 = new Vaisseau("Appolo","Transport");
    Vaisseau vaisseau3 = new Vaisseau("B-wing","Bombardier");
    flotte.addVaisseau(vaisseau1);
    flotte.addVaisseau(vaisseau2);
    flotte.addVaisseau(vaisseau3);
    Operateur op1 = new Operateur("Hugo","Homme","Commandant","Entretien");
    Operateur op2 = new Operateur("Thomas","Homme","Lieutnant","Pilotage");
    Operateur op3 = new Operateur("Julie","Femme","Colonel","Pilotage");
    personnel.addPersonne(op1);
    personnel.addPersonne(op2);
    personnel.addPersonne(op3);
    vaisseau1.addMembre(op1, personnel);
    vaisseau2.addMembre(op2, personnel);
    vaisseau3.addMembre(op3, personnel);
    Libere lib1 = new Libere("Jules","Homme","Commandant");
    Libere lib2 = new Libere("Timon","Homme","Lieutnant");
    Libere lib3 = new Libere("Yoan","Homme","Lieutnant");
    Libere lib4 = new Libere("Thibault","Homme","Colonnel");
    Libere lib5 = new Libere("Jean","Homme","Commandant");
    Libere lib6 = new Libere("Marc","Homme","Lieutnant");
    Libere lib7 = new Libere("Mathilde","Femme","Lieutnant");
    Libere lib8 = new Libere("Lucy","Femme","Colonnel");
    Libere lib9 = new Libere("Marion","Femme","Commandant");
    Libere lib10 = new Libere("Anna","Femme","Lieutnant");
    Libere lib11 = new Libere("Léa","Femme","Lieutnant");
    Libere lib12 = new Libere("Emeline","Femme","Colonnel");
    personnel.addPersonne(lib1);
    personnel.addPersonne(lib2);
    personnel.addPersonne(lib3);
    personnel.addPersonne(lib4);
    personnel.addPersonne(lib5);
    personnel.addPersonne(lib6);
    personnel.addPersonne(lib7);
    personnel.addPersonne(lib8);
    personnel.addPersonne(lib9);
    personnel.addPersonne(lib10);
    personnel.addPersonne(lib11);
    personnel.addPersonne(lib12);
    vaisseau1.addMembre(lib1, personnel);
    vaisseau1.addMembre(lib2, personnel);
    vaisseau1.addMembre(lib3, personnel);
    vaisseau1.addMembre(lib4, personnel);
    vaisseau2.addMembre(lib5, personnel);
    vaisseau2.addMembre(lib6, personnel);
    vaisseau2.addMembre(lib7, personnel);
    vaisseau2.addMembre(lib8, personnel);
    vaisseau3.addMembre(lib9, personnel);
    vaisseau3.addMembre(lib10, personnel);
    vaisseau3.addMembre(lib11, personnel);
    vaisseau3.addMembre(lib12, personnel);
    System.out.println("Vous disposez du personnel le plus compétent, le voici : \n\n"+flotte.displayMembre());
    while(matrice.getNbAgentOperationnel() != 0 && flotte.nbMembreOperationnel() != 0){ // boucle condition de victoire
      System.out.println("Voici le champ de bataille, quels sont vos ordres?\n");
      System.out.println(matrice.toString());
      System.out.println("1) Infiltrer un membre dans la matrice\n2) Exfiltrer un membre de la matrice\n3) Afficher la distance entre un infiltré et l'agent le plus proche");
      boolean check2 = false;
      int entree2 = 0;
      while(! check2){
      entree2 = Clavier.readInt();
        if (entree2 > 0 && entree2 < 4){
          check2 = true;
        }
        else {
          System.out.println("Cette action n'est pas disponible. Veuillez saisir une valeur existante (1 : infiltrer , 2 : exfiltrer , 3 : affichage distance)");
        }
      }//boucle de tant que avant le switch
      switch(entree2){
        case 1 :
          if(entree2==1){
      clearScreen();
            System.out.println("Quel membre infiltrer?");
            System.out.println(flotte.displayInfiltrable());
            System.out.println("Saisissez le numéro du membre.");
            boolean check3 = false;
            int save1 = 0;
            int save2 = 0;
            while(check3 == false){
              int entree3 = Clavier.readInt();
              if (entree3 > 0 && entree3 <= flotte.nbInfiltrable()){
                check3 = true;
                System.out.println(matrice.toString());
                System.out.println("Saisissez la coordonnée x de destination de votre infiltré.");
                boolean check6 = false;
                while(check6 == false){
                  int entree6 = Clavier.readInt();
                  if (entree6 >=0 && entree6 < 10){
                    check6 = true;
                    save1 = entree6;
                    System.out.println("Bien reçu.");
                  }
                  else{
		    System.out.println("Cette coordonnée est hors de la matrice..veuillez saisir un nombre compris entre 0 et 9 inclus.");
                  }
                }
                System.out.println("Saisissez la coordonnée y de destination de votre infiltré.");
                boolean check7 = false;
                while(check7 == false){
                  int entree7 = Clavier.readInt();
                  if (entree7 >=0 && entree7 < 10){
                    check7 = true;
                    save2 = entree7;
                    System.out.println("Bien reçu.");
                    clearScreen();
                  }
                  else{
		    System.out.println("Cette coordonnée est hors de la matrice..veuillez saisir un nombre compris entre 0 et 10 inclus.");
                  }
                }
                if (check6 == true && check7 == true){
                flotte.infiltrerMembre(flotte.getInfiltrable(entree3), matrice, save1, save2);
                System.out.println("Votre membre s'est infiltré dans la matrice !");
                }
              }
              else{
                System.out.println("Ce membre n'existe pas, veuillez saisir un numéro de membre présent dans la liste ci-dessus.");
              }
            }
          }
          break;

        case 2 :
      clearScreen();
          if(entree2==2 && matrice.nbExfiltrable() !=0){
            System.out.println("Voici la liste des membres dans la matrice");
            System.out.println(matrice.displayInfiltre());
            System.out.println("Quel membre exfiltrer parmi ceux infiltrés?");
            boolean check4 = false;
            while(check4 == false){
              int entree4 = Clavier.readInt();
              if(entree4 <= matrice.nbInfiltre() && entree4 >0){
                check4 = true;
                matrice.exfiltrer(matrice.getInfiltre(entree4));
              }
              else{
                System.out.println("Ce membre n'existe pas, concentre toi ! Veuillez saisir un numéro de membre présent dans la liste ci-dessus.");
              }
            }
         }//condition 2 du switch
         else{
            System.out.println("Aucun membre ne peut etre exfiltré, cette fonctionnalité n'est pas encore disponible !");
         }
         break;

        case 3 :
      clearScreen();
          if(entree2==3 && matrice.nbExfiltrable() !=0){
            System.out.println("Voici la liste des membres dans la matrice");
            System.out.println(matrice.displayInfiltre());
            System.out.println("De quel membre voulez-vous connaitre la distance qui le sépare de l'agent le plus proche?");
            boolean check5 = false;
            while(check5 == false){
              int entree5 = Clavier.readInt();
              if (entree5 > 0 && entree5 <= matrice.nbInfiltre()){
                check5=true;
                int distance = matrice.distanceAgent(matrice.getInfiltre(entree5),matrice.agentPlusProche(matrice.getInfiltre(entree5)));
                if(distance != 0){
                  System.out.println("L'agent le plus proche de ce libéré est à" + " " + distance +" cases.");
                }else{
                  System.out.println("La tentative de collecte d'information sur un membre infecté a échoué");
                }
              }
              else{
                System.out.println("Le membre n'existe pas..veuillez saisir un numéro de membre présent dans la liste ci-dessus.");
              }
            }
          }//condition3 du switch
          else {
            System.out.println("Aucun membre n'est infiltré, cette fonctionnalité n'est pas encore disponible !");
          }
          break;
        }//boucle switch

        matrice.promotionAleatoire();

      }//boucle de victoire
     clearScreen();
     if(matrice.getNbAgentOperationnel() == 0){
       System.out.println(" __      ___      _        _          _ ");
       System.out.println(" \\ \\    / (_)    | |      (_)        | |");
       System.out.println("   \\ \\  / / _  ___| |_ ___  _ _ __ ___| |");
       System.out.println("   \\ \\/ / | |/ __| __/ _ \\| | '__/ _ \\ |");
       System.out.println("    \\  /  | | (__| || (_) | | | |  __/_|");
       System.out.println("     \\/   |_|\\___|\\__\\___/|_|_|  \\___(_)");
     }
     else if(flotte.nbMembreOperationnel() == 0){
       System.out.println(" _____    __  __      _ _             ");
       System.out.println("|  __ \\  /_/ / _|    (_) |           ");
       System.out.println("| |  | | ___| |_ __ _ _| |_ ___       ");
       System.out.println("| |  | |/ _ \\  _/ _` | | __/ _ \\    ");
       System.out.println("| |__| |  __/ || (_| | | ||  __/_ _ _ ");
       System.out.println("|_____/ \\___|_| \\__,_|_|\\__\\___(_|_|_)");
     }
   }

   public static void clearScreen() { // méthode nettoyant la console quand elle est appelée.
    System.out.print("\033[H\033[2J");
    System.out.flush();
   }

}
