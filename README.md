# Jeu La Matrice

<div align="center">
<img width="500" height="400" src="La_Matrice.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en DUT INFO 1 à l'IUT de Nantes dans le cadre du module "Bases de la programmation oriéntée objets" durant l'année 2016-2017 avec un groupe de trois personnes. \
Elle consiste à gérer des vaisseaux et des équipages pour plonger dans La Matrice !

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de trois étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Vincent KOWALSKI : vincent.kowalski@etu.univ-nantes.fr ;
- Damien JOUINOT : damien.jouinot@etu.univ-nantes.fr ; 

### Encadrants

Ce projet fut encadré par une enseignante de l'IUT de Nantes :
- Christine JACQUIN : jacquin.christine@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Bases de la programmation oriéntée objets" du DUT INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2017 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 08/04/2017.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Diagramme de classe UML ;
- POO.

## Objectifs

La finalité de ce projet est de pouvoir jouer au jeu La Matrice.
